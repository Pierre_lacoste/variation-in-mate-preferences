# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 16:33:36 2020

@author: Pierre Lacoste
"""

import time
import os

#path = os.path()
t0=time.time()

#Default parameters 
k = 2000
Tintro=1000
phi1=0.9
phi2=0.9
fp="'F'"
h="'T'"
P00 = 0.90
P11 = 0.45
P22 = 0.1
sil=0.9
bic=0.9
tar=0.9

#Path should be define by the user
path = ""

#Read the parameter file.
ref = open(path + "parametre.py","r")
chaine = ref.read()
ref.close()

#Read the main file.
M_ref = open(path + "main.py","r")
script = M_ref.read()
M_ref.close()

#Tested parameters (this is ans exemple, tested paramters should be define by the user)
phi1_test = [0.1,0.5,1]
sil_test = [0.1,0.5,1]
PI_test = [0.1,0.5,1]

#Tested paramters list
parm_test= [phi1_test,sil_test,PI_test]
#Default value for tested parameters
parm_def= [phi1,sil,1]
#Name of tested parameter (should be define by the user)
listparm = ["'phi1","'Sil","'PI"]

#Number of replicates
nrep = 20

## Main loop
a=0
for i in parm_test:
    #parm_test = [0.9]#,0.9,0.9]
    for j in i:
        parm = parm_def
        if j == "'T'":
            string= listparm[a]+"_T'"
        elif j == "'F'":
            string= listparm[a]+"_F'"
        else:
            string= listparm[a]+"_"+str(j)+"'"
        print(string)
        parm[a] = j
        
        print(j)
        
        parametre = open(path + "parametre_auto.py","w")
        parametre.write(chaine.format(parm[0],parm[1],parm[2])) 
        parametre.close()
        
        replicat = 0 
        while replicat < nrep:
            s =  open(path + "/main/main_test"+string+str(replicat)+".py","w")
            s.write(script.format(replicat,string))
            s.close()
            print(replicat)
            runfile(path+"/main/main_test"+string+str(replicat)+".py", wdir = path)
            replicat = replicat + 1
           
    a+=1
    

t1=time.time()

print(t1-t0)




