# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 11:46:18 2020

@author: Pierre Lacoste
"""

"Output management"

# Produce a text file that compile all output produced by the model

import os


dos = os.listdir("./sortie")

#Replace X by a number or a string, the output file name will content this string.
num = "X"

#Choose the table to compile, each output contain 6 files this is the number of the choosen file.
fichier = 3



for i in dos :
    rep = os.listdir("./sortie/"+i)
    t = open("./sortie/"+i+"/data"+num+".txt","w")
    t.close()
    for j in rep[0:(len(rep)-1)]:
        file=open("./sortie/"+i+"/"+j+"/"+os.listdir("./sortie/"+i+"/"+j)[fichier], "r")
        chaine = file.read()
        file.close()
        
        t  = open("./sortie/"+i+"/data"+num+".txt","a")
        t.write("\n"+chaine.split("\n",1)[-1])
        t.close()

if fichier == 3:
    data = open("./data"+num+".txt","w")
    data.write("Temps;Fa;Fb;Fc;Faa;Fab;Fac;Fbc;Fbb;Fcc;FA;FB;FC;N;phi1;phi2;Stade;replicat;parm")
    data.close()
elif fichier == 0:
    data = open("./data"+num+".txt","w")
    data.write("homo;01;02;12;total;temps")
    data.close()
elif fichier == 2:
    data = open("./data"+num+".txt","w")
    data.write("homo;01;02;12;total;temps")
    data.close()




for i in dos:
     t  = open("./sortie/"+i+"/data"+num+".txt","r")
     chaine = t.read()
     t.close()
     
     data = open("./data"+num+".txt","a")
     data.write("\n"+chaine)
     data.close()
