# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 11:40:56 2020

@author: Pierre Lacoste
"""
import numpy as np

"""Parameters"""

"Initial state:"

"Number of Larvaes"
Nl = 8000
"Number of adult of one sex (total number /2)"
Na = 1000
"Initial number of the first mutant"
nmut = 4000
"Carrying capacity"
k=10000

"Number of time step"
Tmax= 300

"Time of introduction of the 2nd mutant"
Tintro=1000

"Parameters for Weibull law"
parm=[4,4]

"Maximum number of larvae produce in each cycle by one female"
nlarve = 5

"Survival matrix"
df = "F"

"Genetic load"
phi0 = 0.05
#phi1 = {0}
phi1 = 0.9
phi2 = 0.8

"Shared load"
fp = "F"

r = 5
#GenoSurv = np.array([[0.95,0.95,0.95],[0.95,0.10,0.95],[0.95,0.95,0.20]])
#sil= {1}
sil= 0.9
bic= 0.9
tar= 0.9
PhenoSurv = [sil,bic,tar]

"Dominance matrix"
matdom = np.array([[0,1,2],[1,1,1],[2,1,2]])

"Disassorative mating"

h = "T"
P00 = 0.1
P11 = 0.1
P22 = 1
P01 = 1
P02 = 1
P12 = 1

cost = "F"
#Reco = "Pheno"
###############################################################################

