# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 15:39:24 2020

@author: Pierre Lacoste
"""
import numpy as np

#Definitions of classes.

class Larve:
    """Class that define Larvae
    
    Atribute:
        Age
        Allele 1
        Allele 2
        Phenotype
    """
    
    def __init__(self, x1, x2, X):
        """ Definition of atributes for the class"""
        
        self.age = 0
        self._x1 = x1
        self._x2 = x2
        self._pheno = X
    
    #We define method to encapsulate attribute.
    def _get_x1(self):
        """Calling method for x1"""
        return(self._x1)
    
    def _get_x2(self):
        """Calling method for x2"""
        return(self._x2)
    
    def _get_geno(self):
        """Calling method for génotype"""
        print("[",self._x1,",",self._x2,"]")
    
    def _get_pheno(self):
        """Calling method for phénotype"""
        
        return (self._pheno)
    
    #These attributes should not be modified so we do not define "set" method.
    
    #We linked our attribute to specific method that read the attribute.
    x1 = property(_get_x1)
    x2 = property(_get_x2)
    geno = property(_get_geno)
    pheno = property(_get_pheno)
    
###############################################################################    

class Male(Larve):
    """ Class that define Male object
        It is a subclass of Larvae
        
        Atribute: 
            Identical to Larvae
            age became a variable insted of constant
            sex"""
    
    def __init__(self, x1, x2, X, age):
        """ Definition of Male attributes """
        
        #Legacy of attribute from the class Larvae.
        Larve.__init__(self, x1, x2, X)
        
        #Attribute of the class.
        self.age = age
        self._sexe ="M"
    
    #Encapsulation of the sex.
    def _get_sexe(self):
        print (self._sexe)    
    sexe = property(_get_sexe)
    
    #Method to count the number of mating.
    def accouplement(self):
        """Number of mating"""
        self.nacc += 1
    
    def viellir(self):
        """Method of ageing"""
        self.age += 1


###############################################################################

class Femelle(Larve):
    """ lass that define Female object
        It is a subclass of Larvae
        
        Atribute: 
            Identical to Larvae
            age became a variable insted of constant
            sex
            Number of mating
            Genotype of the mate
            Number of offspring
            """
    
    def __init__(self, x1, x2, X, age):
        """ Definition of Female attributes """
        
        #Legacy of attribute from the class Larvae.
        Larve.__init__(self, x1, x2, X)
        
        #Attribute of the class.
        self.age = age
        self._sexe = "F"
        self.nacc = 0
        self.malegeno = "NA"
        self.offspring = 0
    
    #Encapsulation of the sex.
    def _get_sexe(self):
        print (self._sexe)   
    sexe = property(_get_sexe)
    
    def viellir(self):
        """Method of ageing"""
        self.age += 1
    
    




        

    
  