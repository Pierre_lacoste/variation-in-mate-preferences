# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 14:17:13 2020

@author: Pierre Lacoste
"""

#Packages 
from __future__ import division
from vpython import combin
import random
from obj import individu as indiv
import numpy as np
import math

###############################################################################
"""Binom"""
def binom(k,n,p):
    """binom(k,n,p): probability to obtain k succes in n independent event with a probaility of p."""
    x=combin(n,k)*pow(p,k)*pow(1-p,n-k)
    return (x)


###############################################################################
"""hbinom"""
def hbinom(n,p,nb=0):
    """hbinom(n,p,nb=0): Random number genrator in binomial distribution"""
    def _hbinom(n,p):
        ph=random.random()
        i=0
        pc=binom(i,n,p)
        while ph>pc:
            i+=1
            pc+=binom(i,n,p)
        return i
    if nb==0:
        return _hbinom(n,p)
    else:
        R=[]
        for j in range(0,nb):
            R.append(_hbinom(n,p))
        return (R)

###############################################################################
"""weibull"""   
def weibull (t,k,l):
    """weibull (t,k,l): Calculate the density probability under a Weibull law of para^meter t,l and k."""
    S = math.exp(-(t/l)**k)
    return (S)

###############################################################################
"""phi"""

def phi (phin,Fxx,Fxy,r):
    """phi (phin,Fxx,Fxy,r): Calculate the genetic load as a function of frequency (unused)"""
    if Fxx > 0:
        phi = phin/(1-(Fxx/(Fxy*r)))
    else:
        phi = phin
    return (phi)

###############################################################################
"""surv"""

def surv (indiv,p):
    """surv (indiv,p): Determine if an inviduals "die" or stay "alive" given a probability p 
    
    Input:    
        indiv: should be an object of the class Larvae or any subclass
        p: is the propability of survive
    Output:
        Object of the same class
        """
    
    #Realisation of a binomial experiment with n=1 and p=p 
    s=hbinom(1,p)
    
    #If it's failure:
    if s==0:
        #then the object is delete and the individuals is consider dead.
        return []
    else:
        #else the object is conserved and the individuals is consider alive.
        return (indiv)


###############################################################################
"""LarveSurv"""
# Fonction LarveSurv => parcoure la liste des larve pour determiner celles qui 
#survivent    
    
def LarveSurv (listeind, genosurv,N ,k ,predprob = 0.1):
    """LarveSurv (listeind, genosurv,N ,k ,predprob = 0.1): determine which larvae survive.
    
    Input:
        listeind: list of object of the Larvae class
        genosurv: matrix of suvival probability (number between 0 and 1)
        N: number of individuals
        k: carrying capacity
        predprob: probability of predation (unused)
    Output:
        list of object from the Larvae class"""
    
    
    liste = []
    
    #For each Larvae in the list
    for i in listeind :
        
        #Calculate the survival probability associate with the genotype:
        p = genosurv[i.x1,i.x2]#-predprob
        
        #Survival test.
        #Correspond to randomly generate a number in a binomial distribution of 
        #parameters n=1 and p=p.
        ind=surv(i,p)
        
        #Only keep individuals that survive.
        if ind != []:
            pc = 1-N/k
            
            if pc<0 :
                ind = []
            else:
                ind = surv(ind, pc)
                if ind != []:
                    #Store the result.
                    liste.append(ind)
                else:
                    continue
        else:
            continue
        
    return (liste)
    
###############################################################################
"""AdultSurv"""
# Fonction LarveSurv => parcoure la liste des adultes pour determiner ceux qui 
#survivent 

def AdultSurv (listeind, genosurv, phenosurv,parm):
    """AdultSurv (listeind, genosurv, phenosurv,parm): determine which Adult (class Male or Female) survive.
    
    Input:
        listeind: list of objects of the Male or Female class
        genosurv: matrix of suvival probability (number between 0 and 1)
        phenosurv: matrix of suvival probability (number between 0 and 1)
        parm: list of paramters for the weibull distribution.
    Output:
        list of object from the Male and Female class"""
    
    
    liste = []
    
    # For i in the list of adults :
    for i in listeind :
        
        #Survival probability as a function of phenotype
        Ppred = phenosurv[i.pheno]
        
        #Survival probability as a function of age
        Pintr = weibull(i.age, parm[0], parm[1])
        
        #Survival test against predation
        #Correspond to randomly generate a number in a binomial distribution of 
        #parameters n=1 and p=Ppred.
        ind = surv(i,Ppred)
        
        #If the individual survive to predation:
        if ind != []:
            
            #Survival test against age.
            #Idem as predation but with n=1 and p=Pintr
            ind = surv(i,Pintr)
            
            # Store only individuals that survive.
            if ind != []:
                ind.viellir ()
                liste.append(ind)
            else:
                continue
        else:
            continue
    return (liste)
    
###############################################################################
"""cycle"""


def cycle (ListeLarve, ListeMale, ListeFemelle, genosurv, phenosurv, N, k, parm):
    """ cycle (ListeLarve, ListeMale, ListeFemelle, genosurv, phenosurv, N, k, parm): Realise a complete life cycle.
    
    Input:
        ListeLarve: List of object from the class Larvae
        ListeMale: List of object from the class Male
        ListeFemelle: List of object from the class Female
        genosurv: matrix of suvival probability (number between 0 and 1)
        phenosurv: matrix of suvival probability (number between 0 and 1)
        N: number of individuals
        k: carrying capacity
        parm: list of paramters for the weibull distribution.
    Output:
        ListeLarve:List of object from the class Larvae
        ListeMale:List of object from the class Larvae
        ListeFemelle:List of object from the class Larvae"""
    
    #Larvae survival
    ListeLarve = LarveSurv(ListeLarve, genosurv, N, k)
    
    #Adult survival
    ListeMale = AdultSurv(ListeMale, genosurv, phenosurv, parm)
    ListeFemelle = AdultSurv(ListeFemelle, genosurv, phenosurv, parm)
    
    #Transformation of all survival larvae into adults
    
    #For each object into the Larvae list 
    for i in ListeLarve :
        #Sex is randomly determine with a probability of 1/2
        sexe = random.randrange(0,2)
        if sexe == 0:
            ind = indiv.Male (i.x1,i.x2,i.pheno, 1)
            ListeMale.append(ind)
        elif sexe == 1:
            ind = indiv.Femelle (i.x1,i.x2,i.pheno, 1)
            ListeFemelle.append(ind)
    ListeLarve = []
    return ([ListeLarve, ListeMale, ListeFemelle]) 
    
   
###############################################################################
"""mating"""
    
def mating (male, femelle, nLarve, matdom):
    """mating (male, femelle, nLarve, matdom): function that produce new Larvae object based on one Female and Male object.
    
    Input:
        male: an object of Male class.
        femelle: an object of Female class
        nLarvae: a number.
        matdom: a matrix that contain dominance relation between each alleles as a number.
    Output:
        A list of object of the class Larvae"""
    
    #Extraction of alleles carried by both the male and the female
    chmale = [male.x1,male.x2]
    chfemelle = [femelle.x1,femelle.x2]
    
    #Pick the number of Larvae produce between 0 and nLarve.
    nb_larve = random.randrange(0,nLarve)
    
    femelle.nacc = 1
    femelle.malegeno = chmale
    femelle.offspring = nb_larve
    #Nex list of Larvae object.
    listbb = []
    
    i=0
    
    #For each new Larvae
    while i < nb_larve:
        
        #2 alleles will be randomly pick in the genotype of both parents.
        x1ind = random.choice(chmale)
        x2ind = random.choice(chfemelle)
        
        #Use the dominance matrix to determine de phenotype of the new individual.
        X = matdom[x1ind,x2ind]
        
        #Create and store the new Larvae object
        ind = indiv.Larve(x1ind,x2ind,X)
        listbb.append(ind)
        
        i += 1
    
    return (listbb)

###############################################################################
"""ponte"""
#Fonction ponte => production de larve à partir de 1 individu (femelle fécondée)

def ponte (Femelle, nLarve, matdom):
    """ponte (Femelle, nLarve, matdom): Function that produce Larvae object from one Female object
    
    Input:
        Femelle: object of female class
        nLarve: number
        matdom: a matrix that contain dominance relation between each alleles as a number.
    Output:
        A list of Larvae object.
        """
    
    chmale = Femelle.malegeno
    chfemelle = [Femelle.x1,Femelle.x2]
    
    nb_larve = random.randrange(0,nLarve)
    
    Femelle.offspring += nb_larve
    listbb = []
    
    i=0
    
    while i < nb_larve:
         
        x1ind = random.choice(chmale)
        x2ind = random.choice(chfemelle)
        
        X = matdom[x1ind,x2ind]
        
        ind = indiv.Larve(x1ind,x2ind,X)
        listbb.append(ind)
        
        i += 1
    
    return (listbb)

###############################################################################
"""heterogamie"""

def heterogamie (P00,P11,P22,P01,P02,P12):
    """heterogamie (P00,P11,P22,P01,P02,P12): build the matrix of mating probability.
    
    Input:
        Probability of mating for each possible pair.
    Output: 
        Matrix of mating probability.
    
    """
    h = np.array([P00,P01,P02,P01,P11,P12,P02,P12,P22])
    h=h.reshape(3,3)
    
    return h
    
###############################################################################
"""repro"""

  
def repro (ListeFemelle , ListeMale, nLarve, h , MatH, matdom, cost):
    """repro (ListeFemelle , ListeMale, nLarve, h , MatH, matdom, cost): function that realise matting.
    
    Input:
        ListeFemelle: List of object from the class Female
        ListeMale: List of object from the class Male
        nLarve: a number.
        h: "T" or "F", if it's "T" there is a mate choice and if it's "F" there is no mate choice.
        MatH: Matrix of mating probability.
        matdom: a matrix that contain dominance relation between each alleles as a number.
        cost: T or F, decide to introduce cost of choice.
    Output:
         Listlarve: List of object of Larvae class
         vrej: vector conataining the number of mate rejection for each possible pair 
         vacc: vector that contain the number of mate acceptance for each possible pair 
        """
    
    Acc = np.array([[0,0,0],[0,0,0],[0,0,0]])
    Rej = np.array([[0,0,0],[0,0,0],[0,0,0]])
    total = 0

    ListeLarve = []
    #For each females
    for i in ListeFemelle : 
        
        #If the female mate previously
        if i.nacc > 0:
            Larve = ponte(i, nLarve, matdom)
        #Else, if the female is unmated 
        else :
            #a male is randomly pick in the list
            j = random.randrange(0,len(ListeMale))
            male = ListeMale[j]
            
            # If there is mate choice
            if h == "T":
                #Mating probability is store from the matrix
               p = MatH[i.pheno,male.pheno]
               #Courtship, realisation of a binomial law with n=1 and p=p
               choice = hbinom(1,p)
               
               #If it's a faillure
               if choice == 0:
                   
                   #register the rejection 
                   Rej[male.pheno,i.pheno] += 1
                   total += 1

                   #Add a cost if cost = "T"
                   if cost == "T":
                       #Go to the next female so the female stay unmated to the next time step.
                       continue
                   #Without cost
                   elif cost == "F" :
                       #While the female stay unmated:
                       while choice == 0:
                            #Same proces than above
                            j = random.randrange(0,len(ListeMale))
                            male = ListeMale[j]
                            p = MatH[i.pheno,male.pheno]
                            choice = hbinom(1,p)
                            Rej[male.pheno,i.pheno] += 1
                            total += 1
                       else :
                            #When the female mate we produce the Larvae
                            Acc[male.pheno,i.pheno] += 1
                            total += 1
                            Larve = mating(male,i, nLarve, matdom)
               elif choice == 1:
                   #Same as above
                   Acc[male.pheno,i.pheno] += 1
                   total += 1
                   Larve = mating(male,i, nLarve, matdom)
            
            #Without mate choice
            elif h == "F":
                Acc[male.pheno,i.pheno] += 1
                Larve = mating(male,i, nLarve, matdom)
        
            
        for j in Larve:
            ListeLarve.append(j)
            
    Vrej = [Rej[0,0]+Rej[1,1]+Rej[2,2],Rej[0,1]+Rej[1,0],Rej[0,2]+Rej[2,0],Rej[1,2]+Rej[2,1],total]
    Vacc = [Acc[0,0]+Acc[1,1]+Acc[2,2],Acc[0,1]+Acc[1,0],Acc[0,2]+Acc[2,0],Acc[1,2]+Acc[2,1],total]
    #print("Rejet\n Homo  01 02 12 Total\n  {0}  {1} {2} {3}  {4}".format(Vrej[0],Vrej[1],Vrej[2],Vrej[3],Vrej[4]))
    #print("Accouplement\n Homo  01 02 12 Total\n  {0}  {1} {2} {3}  {4}".format(Vacc[0],Vacc[1],Vacc[2],Vacc[3],Vacc[4]))

    return ([ListeLarve,Vrej,Vacc])



###############################################################################
"""count"""
#Fonction comptage => retourne les infos importantes 


def count (Listeindiv , compteur, stade):
    """count (Listeindiv , compteur, stade): mesure of geotypic, phenotypic and allelic frequencies.
    
    Input:
        Listeindiv: liste of object from the class Larvae or derivates.
        compteur: Object of the class etat
        stade: "Larvae" or "Adult", define the class used to calcualte frequencies.
    Output:
        Object from the class etat."""
     
    
    #Alleles
    alleff=np.array([0,0,0])
    
    #Phenotypes
    phenoeff=np.array([0,0,0])
    
    #Genotypes
    genoeff=np.array([[0,0,0],[0,0,0],[0,0,0]])
    
    pop = Listeindiv
    
    #Population size 
    N=len(pop)
    compteur.n=N
    
    #For each individuals
    for i in pop:
        
        phenoeff[i.pheno] +=1
        
        alleff[i.x1] += 1
        alleff[i.x2] += 1
        
        genoeff[i.x1,i.x2] += 1
    
    #Frequency calculation and storage in the etat object
    compteur.fa = alleff[0] / (N*2)
    compteur.fb = alleff[1] / (N*2)
    compteur.fc = alleff[2] / (N*2)
    compteur.faa = genoeff[0,0] / N
    compteur.fab = (genoeff[0,1] + genoeff[1,0]) / N
    compteur.fac = (genoeff[0,2] + genoeff[2,0]) / N
    compteur.fbc = (genoeff[1,2] + genoeff[2,1]) / N
    compteur.fbb = genoeff[1,1] / N
    compteur.fcc = genoeff[2,2] / N
    compteur.fA = phenoeff[0] / N
    compteur.fB = phenoeff[1] /N
    compteur.fC = phenoeff[2] /N
    #print(stade,"\n",compteur)
    return (compteur)


###############################################################################
"""chaine"""

def chaine (eff,temps,stade, phi1,phi2):
    """chaine (eff,temps,stade, phi1,phi2): Function that create the string to write the state of the system in a text file.
    
    Input: 
        eff: etat object.
        temps: Time step.
        stade: "Larvae" or "Adult".
        phi1: genetic load on allele 1
        phi2: genetic load on allele 2
    Output: 
        string
        """
    #Initialisation of the string
    line="\n"
    #Add time 
    line+=str(temps)
    #Add attributes of the etat object with ";" as separator.
    line+=";"
    line+=str(eff.fa)
    line+=";"
    line+=str(eff.fb)
    line+=";"
    line+=str(eff.fc)
    line+=";"
    line+=str(eff.faa)
    line+=";"
    line+=str(eff.fab)
    line+=";"
    line+=str(eff.fac)
    line+=";"
    line+=str(eff.fbc)
    line+=";"
    line+=str(eff.fbb)
    line+=";"
    line+=str(eff.fcc)
    line+=";"
    line+=str(eff.fA)
    line+=";"
    line+=str(eff.fB)
    line+=";"
    line+=str(eff.fC)
    line+=";"
    line+=str(eff.n)
    line+=";"
    line+=str(phi1)
    line+=";"
    line+=str(phi2)
    line+=";"
    line+=stade
    line+=";"
    line+=str(eff.replicat)
    line+=";"
    line+=str(eff.parm)
    return (line)

###############################################################################
"""chaine2"""

def chaine2 (liste,nom):
    """chaine2 (liste,nom):function that produce a string with names"""
    
    line="\n"
    j=0
    for i in liste:
        line += nom[j]
        line += " = "
        line += str(i)
        line += "\n"
        line += "\n"
        j += 1
    return (line)

###############################################################################
"""chaine3"""

def chaine3 (V,temps):
    """chaine3 (V,temps): function that produce a string with time and V"""
    line = "\n"
    for i in V:
        line += str(i)
        line += ";"
    line += str(temps)
    return (line)
        




